/**
 * modified by Caven on 2/27: move the original file AccountActivity.java to this file
 * 		and modified the correlated xml files
 */

package com.example.appfinancialplanning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelperAccount;
import com.example.appfinancialplanning.presenter.AccountManager;

/**
 * This activity is reached if user decides to make a new account on current
 * profile.
 */
/**
 * @author Hot Java
 * 
 */
public class AccountCreateActivity extends Activity {
    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_create);
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public final boolean onCreateOptionsMenu(final Menu menu) {
        return true;
    }

    /**
     * Creates the account activity, gets the 4 parameters from the text, then
     * uses the accountManager function to Tell if it is successful. Has several
     * cases if it failed.
     * 
     * @param view
     * 
     */

    /*
     * modified by Caven on 3/10, add protection against invalid strings
     * obtained. examining by string length, and try...catch...
     */

    /**
     * @param view view
     */
    public void onClickMakeAccount(final View view) {
        String accountName = getFullName().getText().toString();
        if (accountName.length() == 0) {
            getHintBar().setText("Invalid account name");
            return;
        }
        String displayName = getDisplayName().getText().toString();
        if (displayName.length() == 0) {
            getHintBar().setText("Invalid display name");
            return;
        }

        String balanceStr = getAccountBalance().getText().toString();

        // Double balance = Double.parseDouble(balance_str);

        Double balance = Double.valueOf(0.0);
        try {
            balance = Double.parseDouble(balanceStr);
        } catch (NumberFormatException nfe) {
            getHintBar().setText("Invalid balance format");
            return;
        }

        String interestRateStr = getInterestRate().getText().toString();
        Double interestRate = Double.valueOf(0.0);
        try {
            interestRate = Double.parseDouble(interestRateStr);
        } catch (NumberFormatException nfe) {
            getHintBar().setText("Invalid interest rate format");
            return;
        }

        // Double interestRate =
        // Double.parseDouble(getInterestRate().getText().toString());
        int result = AccountManager.tryCreateAccount(accountName, displayName,
                balance, interestRate);
        checkResult(result, accountName, displayName, balance, interestRate);
    }
    
    /**
     * 
     * @param result represents constant from create account
     * @param accountName full name
     * @param displayName name shown in UI
     * @param balance amount in account
     * @param interestRate rate of interest
     */
    public void checkResult(int result, String accountName, String displayName,
    						double balance, double interestRate) {
        if (result == 0) {
            // Creation Successful, Account Created
            // Intent intent = new Intent(AccountActivity.this, .class);
            // AccountActivity.this.startActivity(intent);
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Successful");
            // load into the database,too
            DBHelperAccount db = new DBHelperAccount(this);
            db.addAccount(AccountDisplayActivity.username, accountName,
                    displayName, balance, interestRate);

            Intent intent = new Intent(AccountCreateActivity.this,
                    AccountDisplayActivity.class);
            AccountCreateActivity.this.startActivity(intent);
            // backToDisplay();
        } else if (result == 1) {
            // creation unsuccessful, Account already in use
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Account Name already Taken");
        } else if (result == 2) {
            // creation unsuccessful, Balance is negative
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Balance cannot be negative");

        } else if (result == 3) {
            // creation unsuccessful, Interest rate is negative
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Interest Rate cannot be Negative");
        } else if (result == 4) {
            // Creation unsuccessful, Account Name is blank
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Display Name does not exist");
        } else if (result == 5) {
            // Creation unsuccessful, Display Name is blank
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Account Name cannot be blank");
        } else {
            TextView v = (TextView) findViewById(R.id.account_result);
            v.setText("Creation Failed: Form Invalid");
        }
    }

    /**
    private void backToDisplay() {
        Intent intent = new Intent(AccountCreateActivity.this,
                AccountDisplayActivity.class);
        AccountCreateActivity.this.startActivity(intent);

    }
    */

    /**
     * @return hint
     */
    private TextView getHintBar() {
        return (TextView) findViewById(R.id.account_result);
    }

    /**
     * @return fullName
     */
    private EditText getFullName() {
        return (EditText) findViewById(R.id.full_name);
    }

    /**
     * @return displayName
     */
    private EditText getDisplayName() {
        return (EditText) findViewById(R.id.display_name);
    }

    /**
     * @return accountBalance
     */
    private EditText getAccountBalance() {
        return (EditText) findViewById(R.id.account_balance);
    }

    /**
     * @return interestRate
     */
    private EditText getInterestRate() {
        return (EditText) findViewById(R.id.interest_rate);
    }
}