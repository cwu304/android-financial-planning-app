package com.example.appfinancialplanning;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.presenter.UserManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

/**
 * Show acount detail, and add transaction.
 * 
 * @author Chao 2/27/2014
 * 
 */
public class AccountDetailActivity extends Activity {
    /**
     * usermanager.
     */
    private static UserManager userManager;
    /**
     * accountName.
     */
    private String accName;
    /**
     * accountObject.
     */
    private Account acc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Receive", "start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detail);

        userManager = new UserManager(null); // Initialize!
        Intent intent = getIntent();
        accName = intent.getExtras().getString("AccountInfo");
        acc = userManager.getAccount(accName);
        // Account acc = (Account)intent.getSerializableExtra("AccountInfo");
        // accName = acc.getAccountName();

        Log.d("Receive", acc.getAccountName());

        TextView fullName = (TextView) findViewById(R.id.full_name);
        fullName.setText(acc.getAccountName());

        TextView displayName = (TextView) findViewById(R.id.display_name);
        displayName.setText(acc.getDisplayName());

        TextView balance = (TextView) findViewById(R.id.account_balance);
        String bsString = String.valueOf(acc.getBalance());
        balance.setText((bsString));

        TextView intRate = (TextView) findViewById(R.id.interest_rate);
        String itString = String.valueOf(acc.getIntRate());
        intRate.setText((itString));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.account_detail, menu);
        return true;
    }
    
    /**
     * 
     * @param view view
     * @return if click was successful
     */
    public boolean onClickAddTransaction(View view) {

        //String name = (String) ((TextView) view).getText();
        Intent intent = new Intent(AccountDetailActivity.this,
                TransactionActivity.class);

        // Account acc = userManager.getAccount(name);
        // Bundle b = new Bundle();
        // b.putSerializable("AccountInfo", acc);
        intent.putExtra("AccountName", accName);
        // Log.d(name,"Go to Next Act");
        AccountDetailActivity.this.startActivity(intent);
        this.finish();
        // TransactionActivity.finish();
        return true;
    }

    /**
     * 
     * @param view view
     */
    public void onClickBack(View view) {
        Intent intent = new Intent(AccountDetailActivity.this,
                AccountDisplayActivity.class);
        AccountDetailActivity.this.startActivity(intent);
    }

}
