/**
 * created by Caven on 2/27
 */

package com.example.appfinancialplanning;

import java.util.ArrayList;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelperAccount;
import com.example.appfinancialplanning.database.DBHelperTrans;
import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.model.Transaction;
import com.example.appfinancialplanning.presenter.UserManager;

/**
 * Activity defining account display page.
 * @author Hot java
 *
 */
public class AccountDisplayActivity extends Activity {

    /**
     * Object that contains logic for user manipulation.
     */
    private static UserManager userManager;
    /**
     * defines max number of accounts displayed per page.
     */
    public static final int ACC_PER_PAGE = 5;
    /**
     * defines which page user sees first.
     */
    public int pageStart = 1;
    /**
     * username of current user.
     */
    public static String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_display);
        userManager = new UserManager(this);

        String currentUser = userManager.getUserName();
        username = currentUser;

        DBHelperAccount db = new DBHelperAccount(this);
        DBHelperTrans dbTrans = new DBHelperTrans(this);
        ArrayList<Account> accounts = db.getAccounts(username);
        HashMap<String, ArrayList<Transaction>> rawTrans = dbTrans
                .getAllTransactions(username);
        userManager.loadAccounts(accounts, rawTrans);
        Log.d("db", "" + accounts);

        getHintbar().setText(
                currentUser );
        int accountNum = userManager.showAccountNumber();
        getUserbar().setText("You have " + accountNum + " account(s)");

        showPageOfAccounts(accountNum);

    }

    /**
     * show the account from start_page till accNum if not enough accounts to
     * display, hide the rest of accounts on the page.
     * 
     * @param accNum number of account determined by order they are created.
     * 
     **/
    public void showPageOfAccounts(int accNum) {
        Button[] buttons = getDisplayButtons();
        String[] accNames = userManager.getAccountNames();
        for (int i = 0; i < ACC_PER_PAGE; i++) {
            if (pageStart + i > accNum) {
                buttons[i].setVisibility(View.INVISIBLE); // hide
            } else {
                buttons[i].setText(accNames[pageStart + i - 1]);
                // Chao: add onClcik
                // View view = new View();
                // onClickAccountDetail(view);
            }
        }
    }

    /**
     * 
     * @return accounts in terms of buttons to be displayed.
     */
    private Button[] getDisplayButtons() {
        Button[] disp = new Button[ACC_PER_PAGE];
        disp[0] = (Button) findViewById(R.id.account_user_display_1);
        disp[1] = (Button) findViewById(R.id.account_user_display_2);
        disp[2] = (Button) findViewById(R.id.account_user_display_3);
        disp[3] = (Button) findViewById(R.id.account_user_display_4);
        disp[4] = (Button) findViewById(R.id.account_user_display_5);
        return disp;
    }

    /**
     * 
     * @return hintbar
     */
    public TextView getHintbar() {
        return (TextView) findViewById(R.id.account_display_hintbar);
    }

    /**
     * 
     * @return textview
     */
    public TextView getUserbar() {
        return (TextView) findViewById(R.id.account_display_user_bar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.account_display, menu);
        return true;
    }

    // Creat account button
    /**
     * 
     * @param view view
     * @return if click was successful or not (account was created)
     */
    public boolean onClickCreateAccount(View view) {
        Intent intent = new Intent(AccountDisplayActivity.this,
                AccountCreateActivity.class);
        AccountDisplayActivity.this.startActivity(intent);
        return true;
    }

    // Chap 3/19 got to spendCatReport
    /**
     * 
     * @param view view
     * @return if click was successful or not (spending category report shown)
     */
    public boolean onClickSpendingCategoryReport(View view) {
        Intent intent = new Intent(AccountDisplayActivity.this,
                SpendingCategoryReportActivity.class);
        AccountDisplayActivity.this.startActivity(intent);
        // this.finish();
        return true;
    }

    /**
     * 
     * @param view view
     * @return if click was successful or not (shows account in detail)
     */
    public boolean onClickAccountDetail(View view) {
        String name = (String) ((TextView) view).getText();
        Intent intent = new Intent(AccountDisplayActivity.this,
                AccountDetailActivity.class);

        Account acc = userManager.getAccount(name);

        // Bundle b = new Bundle();
        // b.putSerializable("AccountInfo", acc);
        intent.putExtra("AccountInfo", name);
        Log.d(name, "Go to Next Act");
        Log.d(name, String.valueOf(acc.getBalance()));
        AccountDisplayActivity.this.startActivity(intent);
        // this.finish();
        return true;
    }
}
