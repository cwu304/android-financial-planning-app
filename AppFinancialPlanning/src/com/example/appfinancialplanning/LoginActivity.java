package com.example.appfinancialplanning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelper;
import com.example.appfinancialplanning.presenter.LoginManager;

//import com.example.appfinancialplanning.views.EnterUserView;

/**
 * Login Activity that serves as frame for login. Contains edit texts to place
 * login information and private methods to retrieve that information so it can
 * be turned into strings for login attempt.
 * 
 * Uses a LoginManager to handle actual login logic.
 */

public class LoginActivity extends Activity {
    /**
     * name of app.
     */
    public static final String USER_NAME = "com.example.appfinancialplanning.username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    /**
     * attempt login.
     * @param view view
     */
    public void onClickLogin(View view) {
        String username = getUserField().getText().toString();
        String password = getPasswordField().getText().toString();
        // result from attempted login
        int result = LoginManager.tryLogin(username, password,
                MainActivity.accountManager);
        if (result == 0) {
            // login successful, change text
            Intent intent = new Intent(LoginActivity.this,
                    AccountDisplayActivity.class);
            // passing the username is not a good solution! but what else is
            // possible?
            intent.putExtra(USER_NAME, username);
            LoginActivity.this.startActivity(intent);
        } else if (result == 1) {
            // the user might be in the database
            DBHelper db = new DBHelper(this);
            String pwDB = db.getPassword(username);
            if (!pwDB.equals("")) {
                // the user is in the database
                LoginManager.loadIntoMemory(username, pwDB);
                // getHintbar().setText("user loaded into memory;\ntry login again");
                onClickLogin(view);
                return;
            }
            // login unsucessful, username not found
            TextView v = (TextView) findViewById(R.id.login_result);
            v.setText("Login Failed: Username not found");
        } else {
            // login unsscessful, password incorrect
            TextView v = (TextView) findViewById(R.id.login_result);
            v.setText("Login Failed: Incorrect password");
        }
    }

    /**
     * clears memory.
     * @param src src
     */
    public void onClearClicked(View src) {
        LoginManager.clearMemory(MainActivity.accountManager);
    }

    /**
     * 
     * @return edittext (userfield)
     */
    private EditText getUserField() {
        return (EditText) findViewById(R.id.login_name);
    }

    /**
     * 
     * @return password field
     */
    private EditText getPasswordField() {
        return (EditText) findViewById(R.id.password);
    }
}