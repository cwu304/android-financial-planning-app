package com.example.appfinancialplanning;

import java.util.ArrayList;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.appfinancialplanning.database.DBHelperAccount;
import com.example.appfinancialplanning.database.DBHelper;
import com.example.appfinancialplanning.database.DBHelperTrans;
import com.example.appfinancialplanning.presenter.RegistrationManager;

/**
 * Screen where registration takes place.
 */

public class RegisterActivity extends Activity {

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    /**
     * Attempts registration.
     * 
     * @param view view
     */
    public void onClickRegister(View view) {
        String username = getUserField().getText().toString();
        String password = getPasswordField().getText().toString();
        String confirm = getConfirmField().getText().toString();

        /*
         * Chao: check the registration status public static final int
         * NO_PROBLEM=0; public static final int NO_PASSWORD = 1; public static
         * final int USER_EXIST=2; first, check whether Passwords match.
         */
        if (!password.equals(confirm)) {
            // registration unsuccessful,Passwords don't match.
            TextView v = (TextView) findViewById(R.id.register_result);
            v.setText("Registration Failed: Your password and confirmation password do not match!");
        } else {
            // Check if this is in the database
            DBHelper db = new DBHelper(this);
            String pwDB = db.getPassword(username);
            // exam if user is in the database
            if (!pwDB.equals("")) {
                getHintbar().setText("User exists in database");
                return;
            }

            int check = RegistrationManager.tryRegister(username, password,
                    MainActivity.accountManager);
            if (check == 2) {
                // registration unsuccessful, account exist
                TextView v = (TextView) findViewById(R.id.register_result);
                v.setText("Registration Failed: Account already exists");
            }

            if (check == 1) {
                // registration unsuccessful, no password
                TextView v = (TextView) findViewById(R.id.register_result);
                v.setText("Registration Failed: Please input a password");
            }

            if (!password.equals(confirm)) {
                // registration unsuccessful,Passwords don't match.
                check = 4;
                TextView v = (TextView) findViewById(R.id.register_result);
                v.setText("Registration Failed: Your password and confirmation password do not match.");
            }

            if (check == 0) {
                // registration successful, change text
                db.addUser(username, password);

                TextView v = (TextView) findViewById(R.id.register_result);
                v.setText("Registration Successful!");

                // change activity
                Intent intent = new Intent(RegisterActivity.this,
                        LoginActivity.class);
                RegisterActivity.this.startActivity(intent);
            }
        }
    }

    /**
     * 
     * @return textview
     */
    private TextView getHintbar() {
        return (TextView) findViewById(R.id.register_result);
    }
    /**
     * 
     * @return editText
     */
    private EditText getUserField() {
        return (EditText) findViewById(R.id.register_name);
    }
    /**
     * 
     * @return editText
     */
    private EditText getPasswordField() {
        return (EditText) findViewById(R.id.register_password);
    }
    /**
     * 
     * @return editText
     */
    // Chao: get confirmed password
    private EditText getConfirmField() {
        return (EditText) findViewById(R.id.register_confirm);
    }
    /**
     * 
     * @param src view
     */
    public void onClickViewAll(View src) {
        DBHelper db = new DBHelper(this);
        ArrayList<String> users = db.getAllUser();
        getHintbar().setText(
                "Here are the registered users:\n\t" + users.toString());
    }

    /**
     * 
     * @param src view
     */
    public void onClearDB(View src) {
        DBHelper db = new DBHelper(this);
        db.clearUserTable();
        DBHelperAccount dbAcc = new DBHelperAccount(this);
        dbAcc.clearTable();
        DBHelperTrans dbTrans = new DBHelperTrans(this);
        dbTrans.clearTable();
    }

}