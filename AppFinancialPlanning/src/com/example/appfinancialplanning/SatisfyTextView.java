package com.example.appfinancialplanning;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @author Hot Java
 *
 */
class Satisfy_Text_View extends TextView {
    /**
     * @param context context
     */
    /**
     * @param context context
     */
    public Satisfy_Text_View(Context context) {
        super(context);
        init(context);
    }

    /**
     * @param context context
     * @param attrs attrs
     */
    /**
     * @param context context
     * @param attrs attrs
     */
    public Satisfy_Text_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * @param context context
     * @param attrs attrs
     * @param defStyle defStyle
     */
    /**
     * @param context context
     * @param attrs attrs
     * @param defStyle defStyle
     */
    public Satisfy_Text_View(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    /**
     * @param context context
     */
    private void init(Context context) {
        String otfName = "fonts/Satisfy-Regular.ttf";
        Typeface font = Typeface.createFromAsset(context.getAssets(), otfName);
        this.setTypeface(font);
    }
}
