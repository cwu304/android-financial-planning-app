package com.example.appfinancialplanning;

import com.example.appfinancialplanning.helpers.FormatHelpers;
import com.example.appfinancialplanning.presenter.SpendingCategoryReportManager;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 
 * @author Chao 3/19
 * 
 */
public class SpendingCategoryReportActivity extends Activity {
    
    /**
     * @param spendingCategoryReport
     */
    private static SpendingCategoryReportManager spendCatManager;
    
    /**
     * @param report
     */
    String report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spending_category_report);
        spendCatManager = new SpendingCategoryReportManager(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.spending_category_report, menu);
        return true;
    }

    /**
     * @param view view
     */
    public void onClickShowSpendingCategoryReport(View view) {
        // if(true){
        if (getDateStart().getText().length() == 0
                && getDateEnd().getText().length() == 0) {
            report = spendCatManager.createSpendingCategoryReport("01011999",
                    "12312050");
            getHintBar().setText("Spending Category Report  " + report); // +"\""
            // getHintBar().setText("start at " +
            // getDateStart().getText().length()
            // + " end at " + getDateEnd().getText().length());
            return;
        }

        String startString = getDateStart().getText().toString();
        if (!FormatHelpers.dateIsValid(startString)) {
            getHintBar().setText("Date format " + startString + " is invalid");
            return;
        }

        String endString = getDateEnd().getText().toString();
        if (!FormatHelpers.dateIsValid(endString)) {
            getHintBar().setText("Date format " + endString + " is invalid");
            return;
        }
        report = spendCatManager.createSpendingCategoryReport(startString,
                endString);
        getHintBar().setText("Spending Category Report  " + report); // +"\""
    }

    /**
     * @param view view
     */
    public void onClickGoBack(View view) {
        Intent intent = new Intent(SpendingCategoryReportActivity.this,
                AccountDisplayActivity.class); // AccountDisplayActivity
        SpendingCategoryReportActivity.this.startActivity(intent);

    }

    /**
     * @return EditText
     */
    private TextView getHintBar() {
        return (TextView) findViewById(R.id.tra_bar);
    }

    /**
     * @return EditText
     */
    private EditText getDateStart() {
        return (EditText) findViewById(R.id.date_start);
    }
    
    /**
     * @return EditText
     */
    private EditText getDateEnd() {
        return (EditText) findViewById(R.id.date_end);
    }
}
