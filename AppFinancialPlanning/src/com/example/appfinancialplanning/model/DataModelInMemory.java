package com.example.appfinancialplanning.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of DataModel for account manager esque object. Handles the
 * storage of accounts, finding accounts, and adding new accounts.
 * 
 * @author Kevin and Caven
 */

public class DataModelInMemory implements DataModel {

    /**
     * maps name (string) to user objects for easy retrieval of users.
     */
    private Map<String, User> map = new HashMap<String, User>();

    /**
     * constructor for data model. just clears data by default.
     */
    public DataModelInMemory() {
        map.clear();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean addUser(User user) {
        if (map.containsKey(user.getUsername())) {
            return false;
        } else {
            map.put(user.getUsername(), user);
            return true;
        }
    }

    @Override
    public User findUser(String username) {
        if (map.containsKey(username)) {
            return map.get(username);
        } else {
            return null;
        }
    }
}
