package com.example.appfinancialplanning.model;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

/**
 * Transaction Class.
 * 
 * @author Chao 2/27/2014
 * 
 * 
 */

public class Transaction {
    /**
     * date in string form.
     */
    private String dateString; // MMddyyyy
    /**
     * date object.
     */
    private Date date;
    /**
     * amount of a transaction.
     */
    private double amount;
    /**
     * optional note.
     */
    private String note;
    /**
     * Merchant name.
     */
    private String merchant;
    /**
     * type.
     */
    private String type; // Income,Outcome,cash,check,credit,debit
    /**
     * category, descriptor for type.
     */
    private String category; // food, rent, auto, clothing, entertainment
    /**
     * Categories for transaction.
     */
    public static final String[] VALID_CATEGORY = {"food", "rent", "auto",
                                                   "clothing", "entertainment"};
    
    /**
     * 
     * @param amount transaction cost
     * @param dateString date transaction occured
     */
    public Transaction(double amount, String dateString) { // MMddyyyy
        this.dateString = dateString;
        this.amount = amount;
        SimpleDateFormat dateF = new SimpleDateFormat("MMddyyyy", Locale.US);

        try {
            this.date = dateF.parse(dateString);
        } catch (ParseException e) {
            Log.d("dateString convert to date", "Constructor Error!");
        }
    }

    // Chao 3/19
    /**
     * @return transaction information
     */
    public String toString() {
        return category + "    " + amount + "  " + date + "\n";
    }

    /**
     * @return dateString
     */
    public String getDateString() {
        return dateString;
    }

    /**
     * @return date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @return note
     */
    public String getNote() {
        return note;
    }

    /**
     * @return merchant
     */
    public String getMerchant() {
        return merchant;
    }
    /**
     * @return type
     */
    public String getType() {
        return type;
    }
    /**
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /*
     * setters
     */
    /**
     * @param dateString date in string format
     */
    public void setDateString(String dateString) {
        this.dateString = dateString;
    }
    /**
     * 
     * @param dateString date in string format
     */
    public void setDate(String dateString) {
        try {
            SimpleDateFormat dateF = new SimpleDateFormat("MMddyyyy", Locale.US);
            this.date = dateF.parse(dateString);
        } catch (ParseException e) {
            Log.d("dateString convert to date", "setDate Error!");
        }
    }
    /**
     * 
     * @param amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }
    /**
     * 
     * @param noteString to set
     */
    public void setNote(String noteString) {
        this.note = noteString;
    }
    /**
     * 
     * @param merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }
    /**
     * 
     * @param type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * 
     * @param category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

}
