package com.example.appfinancialplanning.presenter;

import com.example.appfinancialplanning.MainActivity;
import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.User;

/**
 * Handles login logic for app. Takes in a login activity and dataModel (what we
 * use to store all the account info). Basically just has a single method to try
 * logging in for now and returns an int based on whether it was sucessful,
 * wrong username, or wrong password.
 * 
 * @author Kevin and Caven
 */

public class LoginManager {
    /**
     * Constant used if username is not found.
     */
    public static final int USERNAME_NOT_FOUND = 1;
    /**
     * constant used if wrong apssword is provided.
     */
    public static final int WRONG_PASSWORD = 2;
    /**
     * constant provided if no problem is found.
     */
    public static final int NO_PROBLEM = 0;
    /**
     * current user after login for account/transaction storage.
     */
    public static User selectedUser;

    /**
     * attempt login.
     * @param username username
     * @param password pw
     * @param model model to store username and pw in
     * @return int based on where login error occured.
     */
    public static int tryLogin(final String username, final String password,
            final DataModel model) {
        User current = model.findUser(username);

        if (current == null) {
            return USERNAME_NOT_FOUND;
        } else if (!current.getPassword().equals(password)) {
            return WRONG_PASSWORD;
        } else {
            AccountManager.current = current;
            selectedUser = current;
            return NO_PROBLEM;
        }
    }

    /**
     * 
     * @param model model to clear.
     */
    public static void clearMemory(final DataModel model) {
        model.clear();
    }

    // load the user from db into the memory
    /**
     * 
     * @param username username
     * @param pw pw
     */
    public static void loadIntoMemory(final String username, String pw) {
        MainActivity.accountManager.addUser(new User(username, pw));
    }

}
