package com.example.appfinancialplanning.presenter;

import com.example.appfinancialplanning.model.DataModel;
import com.example.appfinancialplanning.model.User;

/**
 * Handles registration logic for the app.
 */

public class RegistrationManager {
    /*
     * Chao: Several cases for registration
     */
    /**
     * constant if no problem ios found.
     */
    public static final int NO_PROBLEM = 0;
    /**
     * constant for no password error.
     */
    public static final int NO_PASSWORD = 1;
    /**
     * constant someone tries to register with same name as current user.
     */
    public static final int USER_EXIST = 2;
    
    /**
     * attempts registration.
     * @param username desired name to register
     * @param password desired password for username
     * @param model backing storage for accounts
     * @return int based on if the registration was error free
     */
    public static int tryRegister(final String username, final String password,
            final DataModel model) {
        // Chao: if there is no password, return NO_PASSWORD = 1
        if (password.length() == 0) {
            return NO_PASSWORD;
        } else if (model.addUser(new User(username, password))) {
            return NO_PROBLEM;
        }
        return USER_EXIST;
    }
}
