/*
 * Caven's individual work for M
 * 
 * t
 */



package com.example.appfinancialplanning.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.example.appfinancialplanning.model.User;
import com.example.appfinancialplanning.presenter.AccountManager;
/**
 * junit tests for creating account.
 * @author Hot Java
 *
 */
public class CreateAccountTest {
	/**
	 * Setup of junit test.
	 */
	@Before
	public void setup() {
		AccountManager.current = new User("demo", "pass");
	}
	
	/**
	 * branch coverage junit test for creating acocunt.
	 */
	@Test
	public void test() {
		assertEquals(AccountManager.tryCreateAccount("", "Caven", 1000.0, 0.0), AccountManager.INVALID_FULL_NAME);
		assertEquals(AccountManager.tryCreateAccount("Caven", "", 1000.0, 0.0), AccountManager.INVALID_DISPLAY_NAME);
		assertEquals(AccountManager.tryCreateAccount("Caven", "Caven", -1000.0, 0.0), AccountManager.NEGATIVE_BALANCE);
		assertEquals(AccountManager.tryCreateAccount("Caven", "Caven", 1000.0, -0.1), AccountManager.NEGATIVE_INTEREST_RATE);
		assertEquals(AccountManager.tryCreateAccount("Caven", "Caven", 1000.0, 0.0), AccountManager.NO_PROBLEM);
		assertEquals(AccountManager.tryCreateAccount("Caven", "Caven", 1000.0, 0.0), AccountManager.ACCOUNT_EXISTS);
	}
	
}
