package com.example.appfinancialplanning.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

//import android.util.Log;

import com.example.appfinancialplanning.model.Account;
import com.example.appfinancialplanning.presenter.TransactionManager;


public class CreateTransactionTest {
	public TransactionManager manager;
	
	@Before
	public void setup(){
		manager=new TransactionManager(new Account("demo", "demo", 1000.0, 0.0));
	}
	
	@Test
	public void test() {
		assertEquals(manager.tryCreateTransaction(0, "03252014", "food", ""), TransactionManager.INVALID_AMOUNT);
		assertEquals(manager.tryCreateTransaction(-10, "", "food",""),TransactionManager.INVALID_DATE);
		assertEquals(manager.tryCreateTransaction(-10.0, "03252014", "food", ""),TransactionManager.NO_PROBLEM);
	}

}
