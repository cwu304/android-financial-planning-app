# README #

    •Let's play with Android! Developed An Android Financial Planning App 
    •The App enables user registration/login, create several accounts
    •In every account, users can set balance and rate, add transactions including expenditure / deposit, amount, date, cartegory
    •Form a report for the user to check history expenditure over a given duration for a category (eg. food, rent etc.)
    •Based on Model-View-Presenters design principle, test the code with JUNIT
    •Added GUI enhancements e.g. animation for better user experiences
    •Realized persistency by saving/loading the data using SQLite

##You want to improve this code?
Welcome! 
##These may help:
http://developer.android.com/sdk/installing/index.html  
https://www.genymotion.com/#!/